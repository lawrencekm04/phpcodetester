<!--
=========================================================
* unsafephpcodetester- v1.0.0
=========================================================

* Product Page: https://www.wezadata.co.ke
* Copyright 2019 Wezadata (https://www.wezadata.co.ke)
* License: FREE

* Coded by Lawrence Njenga

=========================================================
 -->

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <title>UNSAFE PHP CODE TESTER</title>
</head>

<body>

  <div class="container m-5 p-5">
    <div class="mb-3 row text-center">
      <h5 class="ml-4">PHP CODE TESTER </h5>&nbsp;&nbsp;
      <small id="resultHelp" class="form-text text-danger mb-1">It is highly unsafe, therefore, use on local computer only</small>
    </div>


    <div class="">
      <form id="code_form" method="POST">

        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">My Code</label>
          <div class="col-sm-10">
            <small id="resultHelp" class="form-text text-danger mb-1">DONT include the opening < ?php tag for your code to run </small> 
            <textarea class="form-control bg-dark text-white" id="inputEmail3" name="code" rows="6"></textarea>
                <small id="emailHelp" class="form-text text-muted">enter your PHP code above and press the RUN button.
                  Drag to expand the text-box from the bottom right to enter more lines of code </small>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-2"></div>
          <div class="col-sm-10">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="gridCheck1" checked>
              <label class="form-check-label" for="gridCheck1">
                I know what am doing
              </label>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-success">RUN</button>
          </div>
        </div>
      </form>
    </div>
    <div class="mb-2">

<div class="form-group row">
  <label for="resultBox" class="col-sm-2 col-form-label"></label>
  <div class="col-sm-10">
    <small id="resultHelp" class="form-text text-muted mb-1">your code result will show here!<span class="glyphicon glyphicon-hand-down"></span></small>

    <textarea class="form-control" id="resultBox" name="code" readonly></textarea>
  </div>
</div>
</div>
    <small id="emailHelp" class="form-text text-muted text-center">
      <a href="mailto:Lawrence@wezadata.co.ke?Subject=Hello%20Lawrence" target="_top">e Mail me for my recklessness</a>
      &nbsp;&nbsp;created for Learning purposes</small>

  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <script>
    $().ready(function() {


      // Variable to hold request
      var request;

      // Bind to the submit event of our form
      $("#code_form").submit(function(event) {

        // Prevent default posting of form - put here to work in case of errors
        event.preventDefault();

        // Abort any pending request
        if (request) {
          request.abort();
        }
        // setup some local variables
        var $form = $(this);
        // Let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");
        // Serialize the data in the form
        var serializedData = $form.serialize();
        // Let's disable the inputs for the duration of the Ajax request.
        // Note: we disable elements AFTER the form data has been serialized.
        // Disabled form elements will not be serialized.
        $inputs.prop("disabled", true);
        // Fire off the request to process_code.php
        request = $.post('process_code.php', serializedData, function(response) {
          // Log the response to the console
          console.log("Response: " + response);
          $('#resultBox').val(response);
        });
        // Let's re-enable the inputs
        $inputs.prop("disabled", false);
      });
    });
  </script>
</body>

</html>