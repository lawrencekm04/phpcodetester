## About unsafephpcodetester

=========================================================

- # unsafephpcodetester- v1.0.0

- Product Page: https://www.wezadata.co.ke
- Copyright 2019 Wezadata (https://www.wezadata.co.ke)
- License: FREE

- Coded by Lawrence Njenga

=========================================================

- This is an Amatuerish Tool.
  This tool is intented for personal use only. Do not host it in your public server !
  We do not offer any guarantees and you need to improve it to fit your needs.
  It is basically reckless,error prone, but, hey, sometimes we just need to get some code tested quickly,
  If hosted on your online server, you could have someone, or yourself compromise or delete all your files.
  This is purely experimental, just use the recommended bash/cmd. With that said, lets install!

#INSTALLING  
(Note, you can just grab the index.php and process_code.php, and run it however you run your php files
provided the two files remain in the same folder. If running ubuntu, you can follow the following 8 steps to access it easily forever.)

#1. Extract the unsafephpcodetester.zip to get a folder named unsafephpcodetester
#2. Place the unsafephpcodetester folder in root directory of your server,
if using apache,place it in /var/www/html/unsafephpcodetester
if using wamp on windows, place it in your www folder C:\wamp\www\unsafephpcodetester
#3. Set up your virtual hosts file so that it points to this new local website.
Add the following content to the file: /etc/apache2/sistes-available/unsafephpcodetester.conf

<VirtualHost \*:80>
ServerName unsafephpcodetester.com
ServerAlias www.unsafephpcodetester.com

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/unsafephpcodetester

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

#4. restart apache2 to update the changes by running the command

    $sudo service apache2 restart

5. Create a simlink to it, in the sites-enabled folder, using the command:
   \$sudo ln -s /etc/apache2/sistes-available/unsafephpcodetester.conf /etc/apache2/sistes-enabled/unsafephpcodetester.conf

#6.Add the following two lines to /etc/hosts

127.0.0.1 unsafephpcodetester.com
127.0.0.1 www.unsafephpcodetester.com

#7. Then on your browser, in the URL address, type  
 http://unsafephpcodetester.com

#8. You are in ! Bookmark it so that you can just click it from your browser bookmarks tab anytime to test some php code
